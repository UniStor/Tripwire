# Tripwire

Management UI in a VM! - Host is ultra secure: The moment any port is plugged in, unplugged, or changed in Cluster: all Hosts kill their VM's, unmounts UniStor, Wipes all Encryption keys - scramble the memory - and Hard Shutdown! all in about 5 seconds

All commands must pass over the network through a Firewall! There is no other way to change anything without Tripwire triggering an immediate shutdown!